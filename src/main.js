import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Axios from 'axios'
import Home from './assets/js/components/Home'
import PokemonList from './assets/js/components/PokemonList'
import PokemonDetail from './assets/js/components/PokemonDetail'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/pokemon', component: PokemonList, name: 'PokemonList' },
    { path: '/pokemon/:name', component: PokemonDetail , name: 'PokemonDetailRouter'},
  ]
});

Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
Vue.prototype.$ajax = Axios


new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
